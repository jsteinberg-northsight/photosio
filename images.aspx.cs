﻿using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using Amazon.S3;
using Amazon.S3.Model;
using System.Configuration;
using System.Text;
using sforce;
using AllYearWebServices;
using System.Web.Services;
using System.Collections.Specialized;

public partial class images : System.Web.UI.Page
{
    protected static string _SalesforceUserId = ConfigurationSettings.AppSettings["_SalesforceUserId"];
    protected static string _SalesforcePassword = ConfigurationSettings.AppSettings["_SalesforcePassword"];
    protected string _SecretAccessKey = ConfigurationSettings.AppSettings["_SecretAccessKey"];
    protected string _AWSAccessKeyId = ConfigurationSettings.AppSettings["_AWSAccessKeyId"];
    protected string _ImageUploaderKey = ConfigurationSettings.AppSettings["_ImageUploaderKey_Production"];
    protected string _S3Bucket = ConfigurationSettings.AppSettings["_S3Bucket"];
    protected static string _S3Host = ConfigurationSettings.AppSettings["_S3Host"];

    public string OuputImageHtml()
    {
        string url;

        NameValueCollection requestForm = Request.Form;
        string _Reference_Number = requestForm["referencenumber"];
        string _Work_Ordered = requestForm["workordered"];
        string _Case_Number = requestForm["casenumber"];
        string _Street_Address = requestForm["streetaddress"];
        string _CaseId = requestForm["caseid"];
        string _ReturnUrl = requestForm["returnUrl"];

        string S3Host = ConfigurationSettings.AppSettings["_S3Host"];
        string S3Bucket = ConfigurationSettings.AppSettings["_S3Bucket"];
        int columns = 0;
        int MaxColumns = 4;
        string HTML = "";

        //Dont do anything if there are no parameters
        if (_Reference_Number != null)
        {
            AmazonS3Client amazonClient = new AmazonS3Client(
                                                    ConfigurationSettings.AppSettings["_AWSAccessKeyId"],
                                                    ConfigurationSettings.AppSettings["_SecretAccessKey"]);

            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = S3Bucket;
            request.Prefix = _Reference_Number;

            ListObjectsResponse response = amazonClient.ListObjects(request);

            //Response.Write("Reference #: " + _Reference_Number);
            Response.Write("Work Ordered: " + _Work_Ordered + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Work Order #: " + _Case_Number + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Street Address: " + _Street_Address);
            
            //Store CaseId
            Response.Write("<input type=\"hidden\" value=\"" + _CaseId + "\" id=\"hidden_caseid\"/>");
            Response.Write("<input type=\"hidden\" value=\"" + _ReturnUrl + "\" id=\"hidden_returnUrl\"/>");

            foreach (S3Object entry in response.S3Objects)
            {
                //check if object is an image
                if (entry.Key.ToLower().Contains(".jpg") || entry.Key.ToLower().Contains(".png") || entry.Key.ToLower().Contains(".bmp") || entry.Key.ToLower().Contains(".gif"))
                {
                    //Write row if first column
                    if (columns == 0)
                    {
                        //Write Row
                        HTML = HTML + "<tr>" + Environment.NewLine;
                    }

                    url = S3Host + "/" + entry.Key;

                    //Add Picture
                    HTML = HTML + "<td align=\"center\">";
                    HTML = HTML + "<img border=\"1\" width=200px src=\"" + url + "\"/>";
                    HTML = HTML + "<BR>";
                    HTML = HTML + "<input id=\"" + entry.Key + "\" type=\"checkbox\">";
                    HTML = HTML + "</td>" + Environment.NewLine;

                    columns++;

                    //If more than max columns, time for a new row
                    if (columns >= MaxColumns)
                    {
                        columns = 0;
                        HTML = HTML + "</tr>" + Environment.NewLine;
                        HTML = HTML + "<tr>";
                        HTML = HTML + "<td><br><td>";
                        HTML = HTML + "</tr>" + Environment.NewLine;
                    }
                }
            }
            amazonClient.Dispose();
        }

        return HTML;
    }

    [WebMethod]
    public static string DeleteImages(string[] aKeyArray)
    {
        //1st element reserved for Case ID
        string caseId = aKeyArray[0];

        //Delete from S3
        try
        {
            string S3Bucket = ConfigurationSettings.AppSettings["_S3Bucket"];

            AmazonS3Client amazonClient = new AmazonS3Client(
                                        ConfigurationSettings.AppSettings["_AWSAccessKeyId"],
                                        ConfigurationSettings.AppSettings["_SecretAccessKey"]);
            
            for (int i = 1; i < aKeyArray.Length; i++)
            {
                
                DeleteObjectRequest request = new DeleteObjectRequest
                {
                    BucketName = S3Bucket,
                    Key = aKeyArray[i]
                };

                DeleteObjectResponse response = amazonClient.DeleteObject(request);
            }
        }

        catch (Exception ex)
        {
            return "Error deleting images from S3 - " + ex.Message.ToString();
        }

        //Delete from Salesforce
        try
        {
            /*
            sforce.SforceService sforceService = new sforce.SforceService();
            sforce.LoginResult login = new sforce.LoginResult();
            login.serverUrl = "https://cs1.salesforce.com";

            login = sforceService.login(_SalesforceUserId, _SalesforcePassword);

            sforceService.Url = login.serverUrl;
            sforceService.SessionHeaderValue = new sforce.SessionHeader();
            sforceService.SessionHeaderValue.sessionId = login.sessionId;

            //Build where clause
            string sql = "select id from ImageLinks__c where ";
           
            for (int i = 1; i <= aKeyArray.Length - 1; i++)
            {
                sql = sql + "(ImageUrl__c = '" + _S3Host + "/" + aKeyArray[i] + "')";
                if (i < aKeyArray.Length - 1)
                {
                    sql = sql + " OR ";
                }
            }

            //Get Ids
            sforce.QueryResult qresults = new sforce.QueryResult();
            qresults = null;
           
            sforceService.QueryOptionsValue = new QueryOptions();
            sforceService.QueryOptionsValue.batchSize = 250;
            sforceService.QueryOptionsValue.batchSizeSpecified = true;

            
            qresults = sforceService.query(sql);
            string[] Ids = new string[qresults.records.Length];

            for (int i = 0; i < qresults.records.Length; i++)
            {
                ImageLinks__c ImageLink = new ImageLinks__c();
                ImageLink = (ImageLinks__c)qresults.records[i];
                Ids[i] = ImageLink.Id;
            }
              
            DeleteResult[] deleteResults = sforceService.delete(Ids);

            if (deleteResults[0].success)
            {
                return "Images deleted.";
            }
            else
            {
                return "Images failed deletion";
            }
            */

            return "Images deleted.";
        }
        catch (Exception ex)
        {
            return "Image deleted from S3, but an error was thrown when trying to delete from Salesforce - " + ex.Message;
        }
    }
}

