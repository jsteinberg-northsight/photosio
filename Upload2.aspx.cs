using System;
using System.Net;
//using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using sforce;
using AllYearWebServices;
using System.Web.Services;
using System.Configuration;
using System.Web.Script.Services;
using System.IO;


public partial class Upload2 : System.Web.UI.Page {
    public String referenceNumber;
    public String loanType;
    public String caseNumber;
    public String caseId;
    public String address;
    public String city;
    public String state;
    public String zip;
    public String AWSAccessKeyID;
    public String AWSSecretAccessKey;
    public String BucketName;
    public String PolicyDocument;
    public String PolicyDocumentSignature;

    protected void Page_Load(object sender, EventArgs e)
    {
        referenceNumber = Request.QueryString["referencenumber"].ToString();
        loanType = Request.QueryString["loantype"];
        caseNumber = Request.QueryString["casenumber"].ToString();
        caseId = Request.QueryString["caseid"].ToString();
        address = Request.QueryString["address"];
        city = Request.QueryString["city"];
        state = Request.QueryString["state"];
        zip = Request.QueryString["zip"];

        AWSAccessKeyID = "AKIAX2SGS2HT6O5VFT4G";
        AWSSecretAccessKey = "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";
        BucketName = "allyearimages";

        DateTime ExpirationDate;
        ExpirationDate = DateTime.UtcNow.AddMinutes(60);

        PolicyDocument = "";
        PolicyDocument += "{";
        PolicyDocument += "  \"expiration\": \"" + ExpirationDate.ToString("s") + ".000Z\",";
        PolicyDocument += "  \"conditions\": [";
        PolicyDocument += "    {\"bucket\": \"" + BucketName + "\"},";
        PolicyDocument += "    {\"acl\": \"public-read\"},";
        PolicyDocument += "    {\"success_action_status\": \"201\"},";
        PolicyDocument += "    [\"starts-with\", \"$Content-Type\", \"image/\"],";
        PolicyDocument += "    [\"starts-with\", \"$Filename\", \"\"],";
        PolicyDocument += "    [\"starts-with\", \"$name\", \"\"],";
        PolicyDocument += "    [\"starts-with\", \"$key\", \"\"],";
        PolicyDocument += "    [\"starts-with\", \"$x-amz-meta-mytag\", \"\"],";
        PolicyDocument += "  ]";
        PolicyDocument += "}";

        Byte[] ByteArray;
        ByteArray = System.Text.Encoding.UTF8.GetBytes(PolicyDocument);
        PolicyDocument = System.Convert.ToBase64String(ByteArray);
        ByteArray = System.Text.Encoding.UTF8.GetBytes(AWSSecretAccessKey);
        System.Security.Cryptography.HMACSHA1 MyHMACSHA1 = new System.Security.Cryptography.HMACSHA1(ByteArray);
        ByteArray = System.Text.Encoding.UTF8.GetBytes(PolicyDocument);
        Byte[] HashArray;
        HashArray = MyHMACSHA1.ComputeHash(ByteArray);
        PolicyDocumentSignature = System.Convert.ToBase64String(HashArray);
    }
    /*
    public static String otherReferenceNumber;
    public static String caseNumber;
    public static String caseId;
    public static String loanType;
    */

    /*
    protected static string _SalesforceUserId = ConfigurationSettings.AppSettings["_SalesforceUserId"];
    protected static string _SalesforcePassword = ConfigurationSettings.AppSettings["_SalesforcePassword"];

    [ScriptMethod, WebMethod]
    public static string CreateImageLinks(String pReferenceNumber) {
        try {
            sforce.LoginResult login = new sforce.LoginResult();
            sforce.SforceService sforceService = new sforce.SforceService();
            login = sforceService.login(_SalesforceUserId, _SalesforcePassword);
            string _sessionId = login.sessionId;

            CaseImageLinkWebServiceService createImageLink = new CaseImageLinkWebServiceService();
            return "Number of records: " + createImageLink.GetNumberOfImageRecords(pReferenceNumber);
        }
        catch (Exception e) {
            return e.Message;
        }
    }
    */
    /*
    protected void Page_Load(object sender, EventArgs e) {
        otherReferenceNumber = Request["referencenumber"].ToString();
        //caseNumber = Request["casenumber"];
        //caseId = Request["caseid"];
        //loanType = Request["loantype"];
    }
    */


    /*CreateMysqlImageLink
     *20170206 - Tyler H. 
     *  renaming function from CreateUploadImagesLink
     *  this function makes a callout to php endpoint which is responsible for creating "imagelink" records in mysql database
     *  I have found that the callout is failing with 500 error from endpoint and I am not sure how long this has been the case.
     *  
     * 
     * 
     */
    [ScriptMethod, WebMethod]
    public static String CreateMysqlImageLink(String pUrl, String pOriginalFilename, String pCaseId, String pCaseNumber) {
    
        string payload = "{{\"CaseId\":\"{0}\",\"ImageUrl\":\"{1}\",\"Filename\":\"{2}\",\"CaseNumber\":\"{3}\",\"Source\":\"Image Uploader\"}}";
        payload = String.Format(
            payload,
            new string[] {
                pCaseId,
                pUrl + HttpUtility.UrlEncode(pOriginalFilename),
                pOriginalFilename,
                pCaseNumber });

        try {
            System.Net.WebRequest req = System.Net.WebRequest.Create("http://puc-env.elasticbeanstalk.com/imageUpload");
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            payload = HttpUtility.UrlEncode(payload);
            payload = "payload=" + payload;
            req.ContentLength = payload.Length;
            System.IO.Stream dataStream = req.GetRequestStream();
            System.IO.StreamWriter writer = new System.IO.StreamWriter(dataStream);
            writer.Write(payload);
            writer.Close();
            req.GetResponse();
        }
        catch (System.Net.WebException ex)
        {
            //return payload + "  :::  " + ex.Message + "\r\n" + ex.StackTrace + "\r\n ::: " + ex.Response + ex.GetType().ToString();
            return
                "endpoint response: *" + ex.Response + "*\r\n" +
                "endpoint status code: *" + ex.Status + "*\r\n";

        }
        return "Done";
    }
    [ScriptMethod, WebMethod]
    public static void notifySFDC(string refNum, string caseID) {
            string sURL;
            sURL = "http://restmysql.northsight.io?action=pushalls3infotocase&caseID=" + caseID + "&prefix=" + refNum + "&push=true";

            WebRequest wrGETURL;
            wrGETURL = WebRequest.Create(sURL);

            Stream objStream;
            objStream = wrGETURL.GetResponse().GetResponseStream();

        /*
        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
        //var client = new RestClient("http://192.168.0.1");
        var client = new RestClient("https://fiserv3-allyearservices.cs1.force.com/services/apexrest/imageLinks");

        var request = new RestRequest(Method.POST);
        request.RequestFormat = DataFormat.Json;

        Dictionary<string, string> openWith = new Dictionary<string, string>();

        // Add some elements to the dictionary. There are no 
        // duplicate keys, but some of the values are duplicates.
        openWith.Add("refNum", refNum);
        
        request.AddJsonBody(openWith);
        IRestResponse response = client.Execute(request);
         * 
         * 
         */
    }


    /*createSFDCimagelink
     * 20170206 - Tyler H. - creating function.  callout to REST endpoint at salesforce org for creating imagelinks__c record
     * 
     *
     */
    [ScriptMethod, WebMethod]
    public static String createSFDCimageLink(String url)
    {
        try
        {
            const string endpointURL = "http://fiserv3-allyearservices.cs1.force.com/services/apexrest/imageLinks";

            //HttpClient queryClient3 = new HttpClient();
            //string serviceURL3 = endpointURL;
            //string insertPacket = "{caseid:\"500S00000085Of2\"}";
            //StringContent insertString = new StringContent(insertPacket, Encoding.UTF8, "application/json");
            //HttpRequestMessage request3 = new HttpRequestMessage(HttpMethod.Post, serviceURL3);
            //request3.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //request3.Content = insertString;
            //HttpResponseMessage response3 = queryClient3.Send(request3);
            //string result = response3.Content.ReadAsString();



            //string result;
            //string json = "{url:\"" +url + "\"}";
            //using (var client = new WebClient())
            //{
            //    client.Headers[HttpRequestHeader.ContentType] = "application/json";
            //    result = client.UploadString(endpointURL, "POST", json);
            //}
            //string payload = "{caseid:\"500S00000085Of2\"}";
            //System.Net.WebRequest req = System.Net.WebRequest.Create(endpointURL);
            //req.Method = "POST";
            //req.ContentType = "application/json";


            //System.IO.StreamWriter writer = new StreamWriter(req.GetRequestStream());
            //writer.Write(payload);
            //writer.Flush();
            //writer.Close();
            //var resp = req.GetResponse();

            return "createSFDCimageLink finished";
        }
        catch (System.Net.WebException ex)
        {
            //return payload + "  :::  " + ex.Message + "\r\n" + ex.StackTrace + "\r\n ::: " + ex.Response + ex.GetType().ToString();
            return
                "endpoint response: *" + ex.Response + "*\r\n" +
                "endpoint status code: *" + ex.Status + "*\r\n" +
                ex.StackTrace;

        }
        catch (Exception e)
        {
            return e.Message;
        }
    }
}

