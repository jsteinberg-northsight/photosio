﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using Amazon.S3;
using Amazon.S3.Model;
using System.Configuration;
using System.Text;

public partial class DownloadAll : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string WorkOrderNumber = Request["casenumber"];
        ClientScriptManager cs = Page.ClientScript;
        StringBuilder ScriptList = new StringBuilder();
        StringBuilder ScriptEvent = new StringBuilder();
        string SavePath = "c:\\\\AllYearServices";

        if (WorkOrderNumber == null)
            WorkOrderNumber = "1111";
        SavePath = SavePath + "\\\\" + WorkOrderNumber;

        ScriptList.Append("function AddFiles() {");
      //ScriptList.Append("         var dl = document.getElementById(\"DownloadX\");");
        ScriptList.Append("         var du = document.getElementById(\"UDownload\");");
        ScriptList.Append("         try{");

        ScriptList.Append("         du.SetSavePathMode = 3;");
        ScriptList.Append("         du.SavePath = \"" + SavePath + "\";");
        ScriptList.Append("         du.UIShowDownloadProgressDialog = 1;");
        ScriptList.Append("         du.DownloadMode = 2;");
        ScriptList.Append("         du.DownloadUIMode = 1;");
		ScriptList.Append("         du.SimultaneityFileCount = 20;");
        ScriptList.Append("         du.SetProgressDialogBackColor(232, 232, 232);");
        ScriptList.Append("         du.SetProgressDialogFontColor(36, 3, 255);");
        ScriptList.Append("         du.UIProgressDialogResize = 1;");
        ScriptList.Append("         du.UIShowDownloadListView = 1;");
        ScriptList.Append("         du.UIShowStatusText = 1;");
        ScriptList.Append("         du.UIShowMainProgress = 1;");
        ScriptList.Append("         du.UIShowCurrentProgress = 1;");
        ScriptList.Append("         du.UIProgressDialogWidth = 620;");
        ScriptList.Append("         du.UIProgressDialogHeight = 600;");
        ScriptList.Append("         du.UIDownloadListViewHeight = du.UIDownloadListViewHeight * 1;");
        ScriptList.Append("         du.UIDownloadListViewWidth = du.UIDownloadListViewWidth * 1;");
        ScriptList.Append("         du.UIDownloadListViewColumList = \"0;1;2;3;4;5;6;7;8\";");
        ScriptList.Append("         du.AutoCloseProgressDlgAfterFinish = 0;");
        ScriptList.Append("         du.SetLicenseSN(\"FHCS31XA3G9D9859-B1R8B6BMB6BMB5B3-8JGB8W5MEH5QAYCA-5LYEHT2T5RMGA3CQ\");");
        
        DownloadImagesNew(ScriptList);
        //DownloadImagesWithTimeStamp(ScriptList);
		//DownloadImagesWithTimeStampPart2(ScriptList);

        ScriptList.Append("         CreateFolder(\"" + WorkOrderNumber + "\");");

        ScriptList.Append("         du.BeginDownload();");
        //ScriptList.Append("         du.PauseDownload();");    
       
        //ScriptList.Append("         du.ResumeDownload();");
        ScriptList.Append("         }");
        ScriptList.Append("         catch(err){alert(err);}");
        ScriptList.Append("     }");
        
        cs.RegisterClientScriptBlock(this.GetType(), "Add", ScriptList.ToString(), true);

        /*
        ScriptEvent.Append("<script for=\"UDownload\" event=\"DownloadComplete(hr)\">");
        ScriptEvent.Append("window.close();");
        ScriptEvent.Append("</script>");

        cs.RegisterClientScriptBlock(this.GetType(), "Event", ScriptEvent.ToString(), true);
        */
    }

    protected void DownloadImagesNew(StringBuilder aScriptList)
    {
        string url;
        string S3Host = ConfigurationSettings.AppSettings["_S3Host"];
        string S3Bucket = ConfigurationSettings.AppSettings["_S3Bucket"];
        string fileName;
        string SavePath = "c:\\\\AllYearServices";

        WebClient Client = new WebClient();
        try
        {
            string WorkOrderNumber = Request["casenumber"];
            if (WorkOrderNumber == null)
                WorkOrderNumber = "1111";
            
            SavePath = SavePath + "\\\\" + WorkOrderNumber;

            AmazonS3Client amazonClient = new AmazonS3Client(
               ConfigurationSettings.AppSettings["_AWSAccessKeyId"],
               ConfigurationSettings.AppSettings["_SecretAccessKey"]);

            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = S3Bucket;
            request.Prefix = WorkOrderNumber;

            ListObjectsResponse response = amazonClient.ListObjects(request);
            foreach (S3Object entry in response.S3Objects)
            {
                url = S3Host + "/" + entry.Key;
                fileName = entry.Key.Replace(WorkOrderNumber + "/", "");
                aScriptList.Append("         du.AddDownload(\"" + url + "\", \"\", \"" + fileName + "\", 3, \"\", \"\", \"\");");
            }
        }

        catch (Exception ex)
        {
            Response.Write(ex.InnerException.Message);
        }
    }

    protected void DownloadImagesWithTimeStamp(StringBuilder aScriptList)
    {
        string S3Bucket = ConfigurationSettings.AppSettings["_S3Bucket"];
        string serviceUrl = "http://nsprelay-env.elasticbeanstalk.com/viewImage/";
        string fileName;
        string SavePath = "c:\\\\AllYearServices";

        WebClient Client = new WebClient();
        try
        {
            string WorkOrderNumber = Request["casenumber"];
            if (WorkOrderNumber == null)
                WorkOrderNumber = "1111";

            SavePath = SavePath + "\\\\" + WorkOrderNumber;

            AmazonS3Client amazonClient = new AmazonS3Client(
               ConfigurationSettings.AppSettings["_AWSAccessKeyId"],
               ConfigurationSettings.AppSettings["_SecretAccessKey"]);

            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = S3Bucket;
            request.Prefix = WorkOrderNumber;

            ListObjectsResponse response = amazonClient.ListObjects(request);
            string url;
            foreach (S3Object entry in response.S3Objects)
            {
                //url = serviceUrl + entry.Key + "?ts=11-20-1014";
				url = serviceUrl + entry.Key;
                fileName = entry.Key.Replace(WorkOrderNumber + "/", "");
                Response.Write("Url: " + url);
                Response.Write(" Filename: " + fileName);
                aScriptList.Append("         du.AddDownload(\"" + url + "\", \"\", \"" + fileName + "\", 3, \"\", \"\", \"\");");
                break;
            }
        }

        catch (Exception ex)
        {
            Response.Write(ex.InnerException.Message);
        }
    }

	protected void DownloadImagesWithTimeStampPart2(StringBuilder aScriptList)
    {
        string S3Bucket = ConfigurationSettings.AppSettings["_S3Bucket"];
        string serviceUrl = "http://nsprelay-env.elasticbeanstalk.com/viewImage/";
        string fileName;
        string SavePath = "c:\\AllYearServices";
        if (Directory.Exists(SavePath) == false)
        {
            Directory.CreateDirectory(SavePath);
        }
        WebClient Client = new WebClient();
        try
        {
            string WorkOrderNumber = Request["casenumber"];
            if (WorkOrderNumber == null)
                WorkOrderNumber = "1111";

            SavePath = SavePath + "\\" + WorkOrderNumber;
            if (Directory.Exists(SavePath) == false)
            {
                Directory.CreateDirectory(SavePath);
            }
            AmazonS3Client amazonClient = new AmazonS3Client(
               ConfigurationSettings.AppSettings["_AWSAccessKeyId"],
               ConfigurationSettings.AppSettings["_SecretAccessKey"]);

            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = S3Bucket;
            request.Prefix = WorkOrderNumber;

            ListObjectsResponse response = amazonClient.ListObjects(request);
            string url;
            foreach (S3Object entry in response.S3Objects)
            {
                //url = serviceUrl + entry.Key + "?ts=11-20-1014";
				url = serviceUrl + entry.Key;
				fileName = entry.Key.Replace(WorkOrderNumber + "/", "");
                
                using (WebClient webClient = new WebClient())
                {
                    using (Stream stream = webClient.OpenRead(url))
                    {
                        using (Stream file = File.Open(SavePath + "\\" + fileName,FileMode.OpenOrCreate))
                        {
                            
                            CopyStream(stream, file);
                            stream.Dispose();
                            file.Dispose();
                            //Response.Write("Save: " + SavePath + "\\" + fileName);
                            
                        }
                    }
                }
                //fileName = entry.Key.Replace(WorkOrderNumber + "/", "");
                Response.Write("Url: " + url);
                //Response.Write(" Filename: " + fileName);
                aScriptList.Append("         du.AddDownload(\"" + url + "\", \"\", \"" + fileName + "\", 3, \"\", \"\", \"\");");
                //break;
            }
        }

        catch (Exception ex)
        {
            Response.Write(ex.Message);
            Response.Write(ex.StackTrace);
        }
    }
    /// <summary>
    /// Copies the contents of input to output. Doesn't close either stream.
    /// </summary>
    public static void CopyStream(Stream input, Stream output)
    {
        byte[] buffer = new byte[8 * 1024];
        int len;
        while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
        {
            output.Write(buffer, 0, len);
        }
    }
}
