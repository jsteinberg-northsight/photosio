﻿using System;
using System.Collections;
using System.Web.UI;
using System.Net;
using Amazon.S3;
using Amazon.S3.Model;
using System.Configuration;
using Ionic.Zip;

public partial class DownloadCase : System.Web.UI.Page {
    protected void Page_Load(object sender, EventArgs e) {
        string WorkOrderNumber = Request["casenumber"];
        string timeStamp = Request["ts"];
        //leave in for debugging
        //WorkOrderNumber = "02007812";
        if (WorkOrderNumber == null) {
            return;
        }
        DownloadImageWithIconicZip(WorkOrderNumber, timeStamp);
        Page.ClientScript.RegisterOnSubmitStatement(typeof(Page), "closePage", "window.onunload = CloseWindow();");
    }

    protected ArrayList getAllImageLinks(string pWorkOrderNumber, string pTimeStamp, string pS3Bucket, string pServiceUrl) {
        ArrayList imageLinks = new ArrayList();

        try
        {
            AmazonS3Client amazonClient = new AmazonS3Client(
                ConfigurationSettings.AppSettings["_AWSAccessKeyId"],
                ConfigurationSettings.AppSettings["_SecretAccessKey"]);
                
            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = pS3Bucket;
            request.Prefix = pWorkOrderNumber;

            do
            {
                ListObjectsResponse response = amazonClient.ListObjects(request);
                if (response.S3Objects.Count > 0)
                {
                    foreach (S3Object entry in response.S3Objects)
                    {
                        imageLinks.Add(pServiceUrl + entry.Key + "?ts=" + pTimeStamp);
                    }
                }

                if (response.IsTruncated)
                {
                    request.Marker = response.NextMarker;
                }
                else
                {
                    request = null;
                }
            } while (request != null);
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
            Response.Write(ex.StackTrace);
        }
        return imageLinks;
    }
    
    protected void DownloadImageWithIconicZip(string pWorkOrderNumber, string pTimeStamp)
    {
        string urldebug="";
        try
        {
            Response.Clear();
            
            string s3Bucket = ConfigurationSettings.AppSettings["_S3Bucket"];
            //string serviceUrl = "https://allyearimages.s3.amazonaws.com/";
            //http://nspdl.elasticbeanstalk.com/viewImage/02007812/P7300564.JPG?ts=11-13-2015
            string serviceUrl = "http://nspdl.elasticbeanstalk.com/viewImage/";
            string archiveName = pWorkOrderNumber + ".zip";
            Response.ContentType = "application/zip";
            Response.AddHeader("content-disposition", "attachment; filename=" + archiveName);
            if (Request.UserAgent.Contains("MSIE"))
            {
                Response.AddHeader("Content-Transfer-Encoding", "binary");
            }

            byte[] stream;
            using (ZipFile zip = new ZipFile())
            {
                zip.CompressionMethod = CompressionMethod.None;
                zip.CompressionLevel = Ionic.Zlib.CompressionLevel.None;
                zip.ParallelDeflateThreshold = -1;
                WebClient client = new WebClient();
                foreach (string currentUrl in getAllImageLinks(pWorkOrderNumber, pTimeStamp, s3Bucket, serviceUrl))
                {
                    try
                    {
                        urldebug = currentUrl;
                        stream = client.DownloadData(currentUrl);
                        zip.AddEntry(currentUrl.Replace(serviceUrl, "").Replace(pWorkOrderNumber + "/", "").Replace("?ts=" + pTimeStamp, ""), stream);
                    }
                    catch (Exception e) {
                        Console.WriteLine(e);
                    }

                }
                zip.Save(Response.OutputStream);
            }
            Response.End();
        }
        catch (Exception e)
        {
            //throw new Exception(e);
            throw new Exception(@"[URL:" + urldebug + "]", e);
        }
    }
}
