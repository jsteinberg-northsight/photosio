﻿using System;
using System.Web.UI;
using System.Net;
using Amazon.S3;
using Amazon.S3.Model;
using System.Configuration;
using Ionic.Zip;
using System.Drawing.Imaging;
using System.Drawing;
using System.IO;
using System.Collections.Generic;

public partial class DownloadCase : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string WorkOrderNumber = Request["casenumber"];
        string ReferenceNumber = Request["referencenumber"];
        string timeStamp = Request["ts"];
        //leave in for debugging
        WorkOrderNumber = "05559813";
        ReferenceNumber = "05559813";
        timeStamp = "2020-06-12";
        if (WorkOrderNumber == null && ReferenceNumber == null)
        {
            return;
        }
        DownloadImageWithIconicZip(WorkOrderNumber, ReferenceNumber, timeStamp);
        Page.ClientScript.RegisterOnSubmitStatement(typeof(Page), "closePage", "window.onunload = CloseWindow();");
    }

    protected Dictionary<string, byte[]> getAllImageLinks(string pWorkOrderNumber, string pTimeStamp, string pS3Bucket)
    {
        Dictionary<string, byte[]> imageLinks = new Dictionary<string, byte[]>();

        try
        {
            AmazonS3Config config = new AmazonS3Config();
            config.RegionEndpoint = Amazon.RegionEndpoint.USEast1;
            AmazonS3Client amazonClient = new AmazonS3Client(
                ConfigurationManager.AppSettings["_AWSAccessKeyId"],
                ConfigurationManager.AppSettings["_SecretAccessKey"],
                config);

            ListObjectsRequest request = new ListObjectsRequest();
            request.BucketName = pS3Bucket;
            request.Prefix = pWorkOrderNumber;

            do
            {
                ListObjectsResponse response = amazonClient.ListObjects(request);
                if (response.S3Objects.Count > 0)
                {
                    foreach (S3Object entry in response.S3Objects)
                    {
                        GetObjectRequest singleRequest = new GetObjectRequest
                        {
                            BucketName = entry.BucketName,
                            Key = entry.Key
                        };
                        using (var getObjectResponse = amazonClient.GetObject(singleRequest))
                        {
                            MemoryStream stream = new MemoryStream();
                            getObjectResponse.ResponseStream.CopyTo(stream);
                            byte[] bytes = null;
                            if (pTimeStamp == null || pTimeStamp == string.Empty)
                            {
                                bytes = stream.ToArray();
                            }
                            else
                            {
                                bytes = DateStampPictures(pTimeStamp, stream);
                            }
                            string[] fileSplit = entry.Key.Split('/');
                            string fileName = fileSplit[fileSplit.Length - 1];
                            if (imageLinks.ContainsKey(fileName))
                                fileName += "-" + DateTime.Now.Ticks;
                            imageLinks.Add(fileName, bytes);
                        }
                    }
                }

                if (response.IsTruncated)
                {
                    request.Marker = response.NextMarker;
                }
                else
                {
                    request = null;
                }
            } while (request != null);
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
            Response.Write(ex.StackTrace);
        }
        return imageLinks;
    }

    protected void DownloadImageWithIconicZip(string pWorkOrderNumber, string pReferenceNumber, string pTimeStamp)
    {
        string urldebug = "";
        try
        {
            Response.Clear();

            string s3Bucket = ConfigurationSettings.AppSettings["_S3Bucket"];
            string archiveName = pWorkOrderNumber + ".zip";
            Response.ContentType = "application/zip";
            Response.AddHeader("content-disposition", "attachment; filename=" + archiveName);
            if (Request.UserAgent.Contains("MSIE"))
            {
                Response.AddHeader("Content-Transfer-Encoding", "binary");
            }

            using (ZipFile zip = new ZipFile())
            {
                zip.CompressionMethod = CompressionMethod.None;
                zip.CompressionLevel = Ionic.Zlib.CompressionLevel.None;
                zip.ParallelDeflateThreshold = -1;

                foreach (KeyValuePair<string, byte[]> currentUrl in getAllImageLinks(pWorkOrderNumber, pTimeStamp, s3Bucket))
                {
                    try
                    {
                        zip.AddEntry(currentUrl.Key, currentUrl.Value);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }

                }

                if (pWorkOrderNumber != pReferenceNumber)
                {
                    foreach (KeyValuePair<string, byte[]> currentUrl in getAllImageLinks(pReferenceNumber, pTimeStamp, s3Bucket))
                    {
                        try
                        {
                            zip.AddEntry(currentUrl.Key, currentUrl.Value);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                    }
                }

                zip.Save(Response.OutputStream);
            }
            Response.End();
        }
        catch (Exception e)
        {
            throw new Exception(@"[URL:" + urldebug + "]", e);
        }
    }

    protected static byte[] DateStampPictures(string dateToUse, Stream file)
    {
        DateTime dtTimeStamp = DateTime.MinValue;
        DateTime.TryParse(dateToUse, out dtTimeStamp);
        Bitmap bitmap = null;
        SizeF size;
        float fHeight = 0;
        float fWidth = 0;
        int fsize = 25;
        byte[] returnArray;

        string sResult = string.Empty;

        try
        {
            bitmap = (Bitmap)Bitmap.FromStream(file);
            if (dtTimeStamp != DateTime.MinValue)
                sResult = dtTimeStamp.ToString("yyyy-MM-dd");

            size = bitmap.PhysicalDimension;
            int tHeight;
            int tWidth;

            foreach (System.Drawing.Imaging.PropertyItem item in bitmap.PropertyItems)
            {
                PropertyItem modItem = item;
                modItem.Value = new byte[] { 0 };
                bitmap.SetPropertyItem(modItem);
            }

            //lower the font size one at a time until it is down to 13% of the photo height, or 50% of the photo width
            do
            {
                fsize = fsize - 1;
                Graphics DrawGraphics = Graphics.FromImage(bitmap);
                Font DrawFont = new Font("Arial", fsize, FontStyle.Regular);
                tHeight = (int)DrawGraphics.MeasureString(sResult, DrawFont).Height;
                tWidth = (int)DrawGraphics.MeasureString(sResult, DrawFont).Width;
            } while (tHeight > (size.Height * .13) || tWidth > size.Width * .5);

            using (bitmap)
            using (var graphics = Graphics.FromImage(bitmap))
            using (var font = new Font("Arial", fsize, FontStyle.Regular))
            {
                tHeight = (int)graphics.MeasureString(sResult, font).Height;
                tWidth = (int)graphics.MeasureString(sResult, font).Width;
                fWidth = size.Width - (tWidth + (float)(size.Width * .02));
                fHeight = size.Height - (tHeight + (float)(size.Height * .02));
                using (MemoryStream returnStream = new MemoryStream())
                {
                    // Do what you want using the Graphics object here.
                    graphics.DrawString(sResult, font, new SolidBrush(Color.FromArgb(0xf2, 0x00, 0x00)), fWidth, fHeight);

                    // Save the file
                    bitmap.Save(returnStream, ImageFormat.Jpeg);
                    returnArray = returnStream.ToArray();
                }
            }
        }
        catch (Exception ex)
        {
            using (MemoryStream returnStream = new MemoryStream())
            {
                file.Position = 0;
                file.CopyTo(returnStream);
                returnArray = returnStream.ToArray();
            }
        }

        return returnArray;
    }
}
