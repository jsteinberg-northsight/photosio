﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="images.aspx.cs" Inherits="images" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script language="javascript" type="text/javascript">

    // Changes the cursor to an hourglass
    function cursor_wait() 
    {
        //setTimeout("document.body.style.cursor = 'wait'", 10);
        document.body.style.cursor = 'wait'
        document.getElementById('selectall').style.cursor = 'wait';
        document.getElementById('deselectall').style.cursor = 'wait';
        document.getElementById('deleteall').style.cursor = 'wait';
        document.getElementById('back').style.cursor = 'wait';       
    }

    function cursor_clear() 
    {
        document.getElementById('image_body').style.cursor = "default";
    }
    
    function checkall() 
    {
        var elem = document.getElementById('myImages').elements;
        
        for (i = 0; i < elem.length; i++) 
        {
            if (elem[i].type == "checkbox") 
            {
                elem[i].checked = true;
            }
        }
    }

    function uncheckall() 
    {
        var elem = document.getElementById('myImages').elements;

        for (i = 0; i < elem.length; i++) 
        {
            if (elem[i].type == "checkbox") 
            {
                elem[i].checked = false;
            }
        }
    }

    function DeleteImages() 
    {  
        var elem = document.getElementById('myImages').elements;
        var KeyArray = new Array();
        var CaseId;
        
        for (i = 1; i < elem.length; i++)
        {
            if (elem[i].type == 'checkbox')
            {
                if (elem[i].checked == true)
                {
                    KeyArray.push(elem[i].id);
                }
            }
            if (elem[i].type == 'hidden')
            {
                if (elem[i].id == 'hidden_caseid') 
                {
                    KeyArray.push(elem[i].value)
                }
            }
        }
        //Only execute if there is something to delete, exclude Case Id
        if (KeyArray.length > 1) 
        {
            cursor_wait();
            PageMethods.DeleteImages(KeyArray, onComplete, onTimeout, onError);
        }
    }

    function onComplete(result, response, context) 
    {
        //cursor_clear();
        alert(result);

        window.location.reload(true);
    }

    function onTimeout(request, context) 
    {
        alert('Javascript - Request timed out.');
    }

    function onError(objError) 
    {
        alert('Javascript - Error - ' + objError.get_message());
    }

    function redirectSalesforce() 
    {
        window.location = document.getElementById('hidden_returnUrl').value;
    }
    
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
 
<style type="text/css">
html, body 
{
    height: 100%;
}
</style>

</head>
<body id="image_body" >
    <form id="myImages" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"/>

        <table>
            <%Response.Write(OuputImageHtml());%>
        </table>
        <input id="selectall" type="button" value="Select All" onclick="checkall();"/>
        <input id="deselectall" type="button" value="Deselect All" onclick="uncheckall();"/>
        <input id="deleteall" type="button" value="Delete Selected Images" onclick="DeleteImages();"/>
        <input id="back" type="button" value="Back to Salesforce" onclick="redirectSalesforce();"/>
    </form>
</body>
</html>
