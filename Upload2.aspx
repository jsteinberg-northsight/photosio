﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Upload2.aspx.cs" Inherits="Upload2" %>
 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<script runat="server">
 
    //You must upload a crossdomain.xml file to the root of your bucket in
    //order for this example to work. You must make this file public (everyone view permission).
     
 
 
</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<title>Case Number - <%=caseNumber%></title>
 
<style type="text/css">
    body {
        font-family:Verdana, Geneva, sans-serif;
        font-size:13px;
        color:#333;
    }
</style>
 
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/themes/base/jquery-ui.css" type="text/css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>
<script src=" https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js" type="text/javacript"></script>
 
<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" type="text/css" />
<link rel="stylesheet" href="../../Scripts/jquery.ui.plupload/css/jquery.ui.plupload.css" type="text/css" />

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js" type="text/javacript"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<!-- production -->
<script type="text/javascript" src="../../Scripts/plupload.full.min.js"></script>
<script type="text/javascript" src="../../Scripts/jquery.ui.plupload/jquery.ui.plupload.js"></script>
 
</head>
<body>
 
	<h3>Order #: <%=caseNumber%> Address: <%=address%> <%=city%> <%=state%> <%=zip%></h3>
	 
	<div id="uploader">
		<p>You browser doesn't have Flash, Silverlight, Gears, BrowserPlus or HTML5 support.</p>
	</div>

	<script type="text/javascript">
	    var uploadCompleteNotificationSent = false;

	    $(window).unload(function () {
	        var referenceNumber = getUrlParameters("referencenumber", "", true);
	        var caseId = getUrlParameters("caseid", "", true);
	        var caseNumber = getUrlParameters("casenumber", "", true);
	        if (!uploadCompleteNotificationSent) {
	            PageMethods.notifySFDC(referenceNumber, caseId);
	        }
	    });

		// Convert divs to queue widgets when the DOM is ready
		$(function () {
			$("#uploader").plupload({
				runtimes: 'html5',
				url: 'https://<%=BucketName%>.s3.amazonaws.com/',
				max_file_size: '50mb',

				multipart: true,
				multipart_params: {
					'key': '${filename}', // use filename as a key
					'Filename': '${filename}', // adding this to keep consistency across the runtimes
					'acl': 'public-read',
					'Content-Type': 'image/jpeg',
					'success_action_status': '201',
					'AWSAccessKeyId': '<%=AWSAccessKeyID%>',
					'policy': '<%=PolicyDocument%>',
					'signature': '<%=PolicyDocumentSignature%>',
					'x-amz-meta-mytag': 'my tag test'
				},

				// !!!Important!!!
				// this is not recommended with S3, since it will force Flash runtime into the mode, with no progress indication
				//resize : {width : 800, height : 600, quality : 60},  // Resize images on clientside, if possible
				resize: {
					width: 640,
					height: 640,
					quality: 90,
					crop: false // crop to exact dimensions
				},

				// optional, but better be specified directly
				file_data_name: 'file',

				// re-use widget (not related to S3, but to Plupload UI Widget)
				multiple_queues: true,

				// Specify what files to browse for
				filters : [
                    { title: "Image files", extensions: "jpg,jpeg,png" }
				],

				// Rename files by clicking on their titles
				rename: true,

				// Sort files
				sortable: true,

				// Enable ability to drag'n'drop files onto the widget (currently only HTML5 supports that)
				dragdrop: true,

				// Views to activate
				views: {
					list: true,
					thumbs: true, // Show thumbs
					active: 'thumbs'
				},

				// Flash settings
				flash_swf_url: '../../Scripts/Moxie.swf',

				// Silverlight settings
				silverlight_xap_url: '../../Scripts/Moxie.xap',

				// Post init events, bound after the internal events
				init : {
				    BeforeUpload: function(up, file) {
						// Called right before the upload for a given file starts, can be used to cancel it if required
				        var uniqueKey = getUrlParameters("referencenumber", "", true) + "/" + file.name;
						// Update the Key and Filename so that Amazon S3
						// will store the resource with the correct value.
						up.settings.multipart_params.key = uniqueKey;
						up.settings.multipart_params.Filename = uniqueKey;
					},
				    UploadComplete: function (up, files) {
				        //var referenceNumber = getUrlParameters("referencenumber", "", true) + "/";
				        var referenceNumber = getUrlParameters("referencenumber", "", true);
				        var caseId = getUrlParameters("caseid", "", true);
				        var caseNumber = getUrlParameters("casenumber", "", true);
				        
				        PageMethods.notifySFDC(referenceNumber, caseId);
				        uploadCompleteNotificationSent = true;
				        o.each(files, function (file) {
				        
				            //PageMethods.CreateMysqlImageLink('http://allyearimages.s3.amazonaws.com/' + referenceNumber,file.name,caseId,caseNumber,createLinkCallback);
							//PageMethods.createSFDCimageLink('http://allyearimages.s3.amazonaws.com/' + referenceNumber + file.name,createLinkCallback);
							
                            /*
							var img = new o.Image();
							img.onload = function() {
								console.log(img.meta);
								var dataURL = img.getAsDataURL(); // you can pass type and quality as arguments
								fabric.Image.fromURL(dataURL, function(imag) {
									fBookUserImgLoaded =true;
					 
									fBookUserImgOrg = imag;
									img.destroy(); // properly dispose artificial image object
									imag = img = null;
									prepImg();
								});
							};
							// passing bare file object here, although not always equal to native one used by browser
							img.load(file.getSource());
                            */
						});
					}
				}
			});
		});

	    function createLinkCallback(result) {
	       console.log(result);
	    }

	    function getUrlParameters(pParameter, pStaticUrl, pDecode) {
	        /*
             Function: getUrlParameters
             Description: Get the value of URL parameters either from 
                          current URL or static URL
             Author: Tirumal
             URL: www.code-tricks.com
            */
	        var currLocation = (pStaticUrl.length) ? pStaticUrl : window.location.search,
                parArr = currLocation.split("?")[1].split("&"),
                returnBool = true;

	        for (var i = 0; i < parArr.length; i++) {
	            parr = parArr[i].split("=");
	            if (parr[0] == pParameter) {
	                return (pDecode) ? decodeURIComponent(parr[1]) : parr[1];
	                returnBool = true;
	            } else {
	                returnBool = false;
	            }
	        }

	        if (!returnBool) return false;
	    }
	</script>
	<form runat="server">
		<asp:ScriptManager ID="scriptManager" runat="server" EnablePageMethods="true" />
	</form>
</body>
</html>